module.exports = {
    "isProduction":true,
    gzip: true,
    verticals:['flights'],
    hotels : {
        "enable_localities_autosuggest" : false,
        "extranet_display_name" : "Hotel"
    },
    "db": {
            "mongodb": {
                "db_name":"flights_travel_db",
                "db_train_name":"trains_travel_db",
                "servers":[{
                    "host":"172.17.2.157",
                    "port":"27017"
                    },
                    {
                    "host":"172.17.2.95",
                    "port":"27017"
                    },
                    {
                    "host":"172.17.4.123",
                    "port":"27017"
                     },
                    {
                    "host":"172.17.4.99",
                    "port":"27017"
                }],
                "replica_set":"travel",
                "db_options": {
                    db: {
                        native_parser: true,
                        readPreference: 'secondaryPreferred'
                    },
                    replset: {
                        auto_reconnect:false,
                        poolSize: 10,
                        socketOptions: {
                            keepAlive: 1,
                            connectTimeoutMS: 30000
                        }
                    },
                    server: {
                        poolSize: 5,
                        socketOptions: {
                            keepAlive: 1,
                            connectTimeoutMS: 30000
                        }
                    }
                }
            },
            "mysql":{
                "host": "extranet.cyufiargq8ua.ap-southeast-1.rds.amazonaws.com",
                "user": "root",
                "password": "paytm_travel",
                "database": "extranet"
            }
    },
    "cache":{
        "redis":{
                "master":{
                    "host": "172.17.2.189",
                    "port": "6379"
                },
                "slaves":[{
                    "host":"172.17.2.189",
                    "port":"6379"
                }]
        }
    },
    "elasticsearch":{
        "servers":["http://172.17.2.113:9200/","http://172.17.4.246:9200/","http://172.17.4.237:9200/"],
        "flights": {
            "servers":["http://172.17.2.113:9200/","http://172.17.4.246:9200/","http://172.17.4.237:9200/"],
            "index": "flights",
            "airport_type": "airport",
            "airline_type": "airline",
            "promo_banner_type": "promo_banner",
            "partner_type": "partner",
            "airport_count": 136
        },
        "trains": {
            "index": "trains",
            "station_type": "station",
            "count": 8500
        }
    },
    "promosearch" : {
        url: "https://apiproxy.paytm.com/papi/v1/promosearch"
    },
    "irctc": {
            "baseUrl":"https://testngetjp.irctc.co.in/eticketing/webservices/taenqservices",
            "masterid":"b2cms5",
            "subuserid": "b2ctest",
            "password":"Testing1"
    },
    "goibibo":{
            "baseUrl":"https://www.goibibobusiness.com/api/",
            "username":"travelpartners@paytm.com",
            "password":"travel@123"
    },
    "ezeego":{
            "baseUrl":"http://ws.ezeego1.com/axis/services/AirSearchDetails?wsdl",
            "ticketingUrl":"http://projects.ezeego1.com/autoticket_test/authserver.php?wsdl",
            "hotelsBaseUrl":"http://hotelws.ezeego1.com/axis/services/v3hotelservices?wsdl",
            "username" : "travelpartners@paytm.com",
            "password" : "ThuJun18",
            "companyCode" : "PAYTM-RXML",
            "base_AD_discount":0,
            "base_CH_discount":0,
            "base_INF_discount":0,
            "tax_AD_discount":0,
            "tax_CH_discount":0,
            "tax_INF_discount":0
    },
    "cart":{
            "url":"https://cart.paytm.com/",
            "secret":"690mw4l105f61a99844c20ui56ni3s78d"
    },
    "oauth":{
            "url":"https://accounts.paytm.com/"
    },
    "desiya":{
            "username":"testsell",
            "password":"t90za6",
            "propertyId":"1000000002",
            "baseSearchUrl":"http://stage-api.travelguru.com/services-2.0/tg-services/TGServiceEndPoint",
            "baseBookingUrl":"http://stage-api.travelguru.com/services-2.0/tg-services/TGBookingServiceEndPoint"
    },
    "little":{
            "searchUrl":"https://nvy.com/api/hotels",
            "bookingUrl":"https://nvy.com/api/mp/order/create",
            "bookingKey":"87ecb889cd2990b5e4a1cc175f394f53",
    },
    "roomstonite":{
            "baseUrl":"http://rtpaytmprod.azurewebsites.net/DataSync.svc/JSON/",
            "authorization":"r15o13192n920512",
            "rt_username":"travelpartners@paytm.com"
    },
    "oyo":{
        "searchUrl":"http://api.oyorooms.com/api/v2/hotels/get_availability",
        "bookingUrl":"http://api.oyorooms.com/api/v2/vendor/bookings",
        "access_token":"RENnUlRHSE52ekxFd2pLVXF4TXk6Q0FkZjdKQnlpcnlQYXprdkFCVjM="
    },
    "maximojo":{
        "searchUrl":"http://secure.maximojo.com/mantrasdirect/",
        "hoteldetailsUrl":"http://secure.maximojo.com/mantrasdirect/bookingavail",
        "submitbookingUrl":"http://secure.maximojo.com/mantrasdirect/submitbooking",
        "access_token":"SDZIbVM2LXB6a3AyOG81VGJnenc6am93RVF6SzFlLUxFc3hxTk5aWlE=",
        "cancelbookingUrl":"http://secure.maximojo.com/mantrasdirect/cancelbooking",
        "cancellationfeeUrl":"http://secure.maximojo.com/mantrasdirect/getcancellationfee",
        "username":"mantrasdirect@maximojo.com",
        "password":"245618955324985126475114582354586658468445222545658",
        "queryKey":"3cc343af545749818805dd199a914dee_270320151728_1_1",
        "partner_code":"PAYTM",
        "mycountry":"India",
        "booking_request_id":"CMID-9818805dd199a914defe3cc199a",
        "booking_session_id":"545749818805dd199a914dee3cc343af",
        "ip_address":"192.168.1.1"
    },
    "easeMyTrip":{
           "baseUrl":"http://serviceapi.easemytrip.com/air.svc?wsdl",
           "methods" : {
               "search" : "AirSearch",
               "reprice":"AirPriceReq",
               "booking" : "AirBookRQ"
           },
           "username" : "paytm",
           "password" : "Au8hUD3gJ7Xmy8BV",
           "portalId" : "26",
           "airlines" : {
               "Indigo" : 1,
               "AirAsia" : 1,
               "TravelPort" : 1,
               "GoAir" : 1,
               "Spicjet" : 1,
               "Trujet" : 0,
               "Airpegasus" : 0,
               "AirCosta" : 0
           },
           "ttw":30000,
           "xmlns" : {
               "air" : "http://schemas.datacontract.org/2004/07/AirService"
           } 
   },
     "SastiTicket":{
            "baseUrl":"https://www.sastiticket.com/websvc/",
            "path" : {
                "search" : "AirSearchServiceV1?wsdl",
                "reprice" : "AirPriceServiceV1?wsdl",
                "booking" : "CartBookServiceV1?wsdl"
            }, 
            "methods" : {
               "search" : "AirSearchRQ",
               "reprice":"AirPriceRQ",
               "booking" : "CartBookRQ"
           },
           "Username" : "20537",
           "Password" : "Dp3$K3uiZ#84@dDMfd"
            
    },
    "fulfillmentService":{
            "url":"https://fulfillment.paytm.com",
            "service_id":28,
            "flights" : {
                "service_id": 29
            }
    },
    "cancelActionUrl":{
        "url": "http://travel.paytm.com/api/hotels/v1/cancel_book/",
        "dummyUrl": "http://travel.paytm.com/api/hotels/v1/dummy_cancel/"
    },
    "persona":{
            "clientKey":"ffservice-ezeego",
            "clientSecret":"615f46b01208249164e23146db5093144168163f",
            "username":"flights.dev@paytm.com",
            "password":"flights@travel",
            "endpoint":"https://persona.paytm.com",
            "headers": {
                Authorization: 'Basic ZmZzZXJ2aWNlLWV6ZWVnbzo2MTVmNDZiMDEyMDgyNDkxNjRlMjMxNDZkYjUwOTMxNDQxNjgxNjNm'
            },
            "flights":{

                "clientKey":"market-staging",
                "clientSecret":"ca602054-b774-4d64-8478-cecc19b39852",
                "username":"flightdev@paytm.com",
                "password":"flights@paytm",
                "endpoint":"https://persona-dev.paytm.com",
                "headers": {
                    Authorization: 'Basic bWFya2V0LXN0YWdpbmc6Y2E2MDIwNTQtYjc3NC00ZDY0LTg0NzgtY2VjYzE5YjM5ODUy'
                }
            }
    },
    "ttl": {
        "flights":  {
            "search": {
                "today": 15,
                "tomorrow": 30,
                "week": 30,
                "farther": 30
            },
            "search_timeout": 60,
			"search_loadall": 30,
            "update_route_ttl": 5,
            "requestid": 720,
            "reprice": 720,
            "booking": 1440,
            "order": 1440,
            "checkout_timeout": 15
        },
        "hotels":{
            "search":{
                "Goibibo":21600,
                "Oyo":555,
                "Little":555,
                "Roomstonite":555,
                "Ezeego":86400,
                "Desiya":555,
                "Paytm":600,
                "Maximojo":21600
            }
        }
    },
	loadbalancer: {
		enabled: true
	},
	compression: {
		flights: {
			EaseMyTrip: 1,
			SastiTicket: 1
		}
	},
    ota_switches :{
        "hotels":{
            "GOIBIBO":1,
            "ROOMSTONITE":1,
            "EZEEGO":1,
            "DESIYA":0,
            "LITTLE":1,
            "OYO":0,
            "MAXIMOJO":1,
            "PAYTM":1
        },
        "flights": {
            goibibo: 0,
            ezeego1: 0,
            EaseMyTrip: 1,
            SastiTicket :1,

            round_trip_providers : {
                'EaseMyTrip' : 1,
                'SastiTicket': 1,               
                'ezeego1' : 0,
            },
            oneway_providers : {
                'EaseMyTrip' : 1,
                'SastiTicket' : 1,
                'ezeego1' : 0
            }
        }
    },
    "ota_pref":{
        "hotels":{
            "primary":{
                "Goibibo":1,
                "Oyo":1,
                "Little":1,
                "Roomstonite":0,
                "Ezeego":1,
                "Desiya":1,
                "Maximojo":2,
                "Paytm":0
            },
            "secondary":{
                "Paytm":1,
                "Goibibo":2,
                "Oyo":3,
                "Little":4,
                "Roomstonite":5,
                "Ezeego":6,
                "Desiya":7,
                "Maximojo":8
            },
            "mapping":["Paytm","Roomstonite","Little","Oyo","Goibibo","Ezeego","Desiya","Maximojo"]
        }
    },
    digitalProducts:{
        "GOIBIBO":22615408,
        "EZEEGO":22615409,
        "ROOMSTONITE":24404117,
        "DESIYA":505700479,
        "LITTLE":27049840,
        "OYO":32534056,
        "flights": {
            "goibibo": 505705424,
            "ezeego1": 32764869,
            "EaseMyTrip_AI":50846720,
            "EaseMyTrip_UK":50846721,
            "EaseMyTrip_G8":50846722,
            "EaseMyTrip_9W":50846723,
            "EaseMyTrip_I5":50846724,
            "EaseMyTrip_SG":50846725,
            "EaseMyTrip_6E":50846726,
            "EaseMyTrip":50846720, // generic emt product ID,
            "SastiTicket_AI_E" : 55033099,
            "SastiTicket_AI_B" :55033105,
            "SastiTicket_UK_E" : 55033106,
            "SastiTicket_UK_B" : 55033101,
            "SastiTicket_6E" : 55009690,
            "SastiTicket_LB" : 55033097,
            "SastiTicket_SG" : 55033098,
            "SastiTicket_G8" : 55033100,
            "SastiTicket_I5" : 55033103,
            "SastiTicket_9W_CL1" : 55033102,
            "SastiTicket_9W_CL2" : 55033104,
            "SastiTicket_S2_CL1" : 55033102,
            "SastiTicket_S2_CL2" : 55033104, // generic emt product ID
            "SastiTicket":55009690
        }
    },
    digitalCategories: {
        flights: 69089
    },
    allowedDomains: ['https://fe.paytm.com','http://fe.paytm.com', 'https://beta.paytm.com', 'https://beta1.paytm.com', 'https://seller-dev.paytm.com','https://seller.paytm.com','https://merchant-dev.paytm.com','https://merchant.paytm.com','https://paytm.com','https://www.paytm.com','http://paytm.com','http://www.paytm.com','http://travel.paytm.com','http://www.travel.paytm.com'],
    paytmfee: {
        flights: {
            goibibo: {oneway: 200, twoway: 350},
            ezeego1: {oneway: 200, twoway: 350},
            EaseMyTrip: {oneway: 200, twoway: 350},
            SastiTicket: {oneway: 200, twoway: 350}               
        }
    },
    flights: {
        utils:{
            cancel_widget_timelimit:4,
            algorithm:'aes-256-ctr',
            password:'P@ssw0rd',
            "cancelationcharge" : {
                "caneclation": {
                    "EaseMyTrip" : {
                        "charge" : 25
                    },
                    "ezeego1" : {
                        "charge" : 150
                    },
                    "SastiTicket" : {
                        "charge" : 0
                    }
                },
                "minimumfair" : {
                    "otherairlines" : {
                        "charge" : 2250
                    },
                    "goair" : {
                        "charge" : 2225
                    }
                }
            },
            prefix_length:1,
            fuzziness:2
        },
        booking: {
            payment_contact: {
                address: 'B-121, Sector 5',
                city: 'Noida',
                pincode: '201301',
                state: 'Uttar Pradesh',
                payment_mode: 'paytm',
                "ezeego1":{
                    address: 'Vaman Techno Centre, 4th floor, Marol-Makwana Road',
                    city: 'Andheri(E), Mumbai, ',
                    pincode: '400059',
                    state: 'Maharashtra',
                    payment_mode: 'paytm',
                },
                "EaseMyTrip":{
                    address: '223 , F.I.E , Patparganj Industrial Area',
                    city: 'New Delhi',
                    pincode: '110092',
                    state: 'New Delhi',
                    payment_mode: 'paytm',
                },
                "SastiTicket":{
                    address: '904 Rohit House, 3. Tolstoy Marg',
                    city: 'New Delhi',
                    pincode: '110001',
                    state: 'New Delhi',
                    payment_mode: 'paytm',
                }
            }
        },
        callback: {
            cancel_terms_form: 'http://travel.paytm.com/api/flights/v1/order/form/cancel_terms',
            cancel_request: 'http://travel.paytm.com/api/flights/v1/order/item/cancelrequest',
            cancel: 'http://travel.paytm.com/api/flights/v1/order/item/cancel',
            actions: "http://travel.paytm.com/api/flights/v1/order/control_actions"
        },
        provider_order: ['goibibo', 'ezeego'],
        commissionable_components: {
            ezeego: ['YQ'],
        },
        comm_val:{
            tax:{
                EaseMyTrip:1,
                ezeego1:0,
                SastiTicket:0
            },
            yq:{
                EaseMyTrip:0,
                ezeego1:1,
                SastiTicket:1
            }
        }
    },
    images:{
        "hotels":{
            "base":"https://s3-us-west-2.amazonaws.com/paytm-travel/travel_db/hotels/",
            "placeHolderThumb": "https://s3-us-west-2.amazonaws.com/paytm-travel/travel_db/placeHolders/hotels_thumb.png",
            "placeHolderFull": "https://s3-us-west-2.amazonaws.com/paytm-travel/travel_db/placeHolders/hotels_full.png"
        }
    },
    token_keys:{
        "sso" : "OBliWJeeck2SWKDnnnBFYcs4ZE0EkqAgCShJBBUEFA0n",
        "wallet" : "OBliWJeeck2SWKDnnnBFYcs4ZE0EkqAgCShJBBUEFA0n"
    }
};
