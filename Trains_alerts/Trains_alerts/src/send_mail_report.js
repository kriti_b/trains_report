var request = require('request');
var btoa = require('btoa');
var async1 = require('async');
var mongoose = require('mongoose');
var fs = require('fs');
var parse = require('csv-parse');
var dateFormat = require('dateformat');
var CronJob = require('cron').CronJob;
global.__base= __dirname.split('scripts')[0];

var config = require(global.__base + '/config.js');
var config1 = require(global.__base + 'scripts/mongo/hourly/config/config.js');
var nodemailer = require("nodemailer");
var smtpTransport = require('nodemailer-smtp-transport');
exports.send_report_mail = function(text,date,type,cb){
       var options = {
	    "service" : "gmail",
	    "auth" : {
	        "user" : "flight-report@paytm.com",
	        "pass" : "flight#12345"
	    }
    };
	var transporter = nodemailer.createTransport(smtpTransport(options));
	var mail = {};
       // mail.to = config1.ops_mail_id;
        mail.to = config1.trains_email_id;
	mail.html = text;
	mail.template = null;
        if(type == 0){
    
	   mail.subject = 'Trains GMV Monitoring Report';
        }
        else if(type == 2) {
        //    mail.to = ['kriti.bajoria@paytm.com', 'jaijin.austin@paytm.com', 'ravi.pandey@paytm.com','sunil.parolia@paytm.com'];
            mail.subject = 'Trains GMV Monitoring Report';
        }
        else{
         //  mail.to = ['ravi.pandey@paytm.com'];
           mail.subject = 'Promos Monitoring Report - ' + date + ' - Hourly';;
        }
	mail.attachments = null;
	mail.text = null;
	transporter.sendMail(mail,function(error,response){
		if(error){
			console.log('mail has not been send due to',error);
		}
		else{
			console.log('mail has been send');
			transporter.close();
			cb();
		}
	});
}

exports.send_report_mail3 = function(text,text1,text2,date,type,cb){
       var options = {
	    "service" : "gmail",
	    "auth" : {
	        "user" : "flight-report@paytm.com",
	        "pass" : "flight#12345"
	    }
    };
	var transporter = nodemailer.createTransport(smtpTransport(options));
	var mail = {};
       // mail.to = config1.ops_mail_id;
        mail.to = config1.trains_email_id;
	mail.html = text + text1 + text2;
	mail.template = null;
        if(type == 0){
    
	   mail.subject = 'Flights GMV Monitoring Report - Half Hourly';
        }
        else if(type == 2) {
       //   mail.to = ['kriti.bajoria@paytm.com'];
            mail.subject = 'Trains GMV Monitoring Report';
        }
        else{
         //  mail.to = ['ravi.pandey@paytm.com'];
           mail.subject = 'Promos Monitoring Report - ' + date + ' - Hourly';;
        }
	mail.attachments = null;
	mail.text = null;
       	transporter.sendMail(mail,function(error,response){
		if(error){
			console.log('mail has not been send due to',error);
		}
		else{
			console.log('mail has been send');
			transporter.close();
			cb();
		}
	});
}

exports.send_report_mail2 = function(text,text1,date,type,cb){
       var options = {
	    "service" : "gmail",
	    "auth" : {
	        "user" : "flight-report@paytm.com",
	        "pass" : "flight#12345"
	    }
    };
	var transporter = nodemailer.createTransport(smtpTransport(options));
	var mail = {};
       // mail.to = config1.ops_mail_id;
        mail.to = config1.trains_email_id;
	mail.html = text + text1;
	mail.template = null;
        if(type == 0){
    
	   mail.subject = 'Flights GMV Monitoring Report - ' + date + ' - Half Hourly';
        }
        else if(type == 2) {
       //    mail.to = ['kriti.bajoria@paytm.com'];
            mail.subject = 'Trains GMV Monitoring Report - Half Hourly';
        }
        else{
         //  mail.to = ['ravi.pandey@paytm.com'];
           mail.subject = 'Promos Monitoring Report - ' + date + ' - Hourly';;
        }
	mail.attachments = null;
	mail.text = null;
	transporter.sendMail(mail,function(error,response){
		if(error){
			console.log('mail has not been send due to',error);
		}
		else{
			console.log('mail has been send');
			transporter.close();
			cb();
		}
	});
}

exports.send_report_mail1 = function(mail,cb){
       var options = {
	    "service" : "gmail",
	    "auth" : {
	        "user" : "flight-report@paytm.com",
	        "pass" : "flight#12345"
	    }
    };
	var transporter = nodemailer.createTransport(smtpTransport(options));
	transporter.sendMail(mail,function(error,response){
		if(error){
			console.log('mail has not been send due to',error);
		}
		else{
			console.log('mail has been send');
			transporter.close();
			cb();
		}
	});
}
