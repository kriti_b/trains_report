var request = require('request');
var btoa = require('btoa');
var async1 = require('async');
var mongoose = require('mongoose');
var fs = require('fs');
var parse = require('csv-parse');
var dateFormat = require('dateformat');
var CronJob = require('cron').CronJob;
global.__base= __dirname.split('scripts')[0];
var Transaction = require(global.__base + 'data/models/trains_db/transaction.js');
var Booking = require(global.__base + 'data/models/trains_db/booking.js');
var config = require(global.__base + 'scripts/mongo/Trains_alerts/src/config.js');
var mail = require(global.__base + 'scripts/mongo/Trains_alerts/src/send_mail_report.js');
var Template = require(global.__base + 'scripts/mongo/Trains_alerts/src/template_trains.js');
var async = require(global.__base + 'scripts/mongo/Trains_alerts/src/elasticsearch/asynctask1.js');
var bookingdb = require(global.__base + 'scripts/mongo/Trains_alerts/src/trains_booking.js');
function ConString(){
	var index = 0;
	var connection_string = "mongodb://";

	for(index = 1; index <config.db.mongodb.servers.length; ++index) {
		connection_string += config.db.mongodb.servers[index].host 
						+ ":" + config.db.mongodb.servers[index].port;
		connection_string += ',';
	}
	connection_string = connection_string.slice(0, -1);
	connection_string += '/';
	connection_string += config.db.mongodb.db_train_name + '?replicaSet=' + config.db.mongodb.replica_set;
	return connection_string;
}


mongoose.connect(ConString(), config.db.mongodb.db_options,function(err){
});
(function generateDailyReportInTrain(){
  var data = {};
  var past_one_hour_time = new Date();
  past_one_hour_time.setMinutes(past_one_hour_time.getMinutes()-30);
  var past_one_weak = new Date();
  past_one_weak.setHours(0,0,0,0);
  past_one_weak.setDate(past_one_weak.getDate()-7);
  past_one_weak.setHours(past_one_weak.getHours() - 5);
  past_one_weak.setMinutes(past_one_weak.getMinutes() - 30);
  var past_six_day = new Date();
  past_six_day.setDate(past_six_day.getDate()-7);
  var past_one_day = new Date();
  past_one_day.setHours(0,0,0,0);
  past_one_day.setDate(past_one_weak.getDate()-1);
  past_one_day.setHours(past_one_weak.getHours() - 5);
  past_one_day.setMinutes(past_one_weak.getMinutes() - 30);
  var past_zero_day = new Date();
  past_zero_day.setDate(past_six_day.getDate()-1);
  var today_midnight = new Date();
  today_midnight.setHours(0,0,0,0);
  today_midnight.setHours(today_midnight.getHours() - 5);
  today_midnight.setMinutes(today_midnight.getMinutes() - 30);
  var current_time = new Date();
  current_time.setHours(current_time.getHours());
  current_time.setMinutes(current_time.getMinutes());
  var hour = current_time.getHours();
  var past_one_hour = hour -1;
  var date = current_time.toDateString()
  var query = {
      "booking_time" : {
          "$gte" : today_midnight,
          "$lte" : current_time
      }
  };
  var query1 = {
      "booking_time" : {
          "$gte" : past_one_weak,
          "$lte" : past_six_day
      }
  };

  var query2 = {
      "booking_time" : {
          "$gte" : past_one_day,
          "$lte" : past_zero_day
      }
  };
  
  var hour_order =[];
  var html_text = "";
  var till_now_order = [];
  var past_one_day_order = [];
  var past_t_minus_seven_order = [];
  var past_t_minus_seven_hour_order = [];
  var past_t_minus_one_order = [];
  //console.log('******************************',past_one_hour_time);
  try{
    async1.parallel([
      function(callback){
          Transaction.find(query1,function(err,data){
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  data.forEach(function(order){
                      past_t_minus_seven_order.push(order);
                      //if(order.booking_time >= t_past_hour){
                        //   past_t_minus_seven_hour_order.push(order);
                     // }
                  });
                  callback(err,data); 
              }
          });
      },
      function(callback){
          Transaction.find(query,function(err,data){
              console.log(query,"query");
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  data.forEach(function(order){
                      till_now_order.push(order);
                  });
                  for(var i = 0; i< till_now_order.length ; i++){
                      if(till_now_order[i]['booking_time']>=past_one_hour_time){
                          hour_order.push(till_now_order[i]);
                      } 
                      if(till_now_order[i]['booking_time'] >= today_midnight){
                          past_one_day_order.push(till_now_order[i]);
                      }
                  }
                  callback(err,data); 
              }
          });
      },
      function(callback){
          Transaction.find(query2,function(err,data){
              console.log(query2,"query");
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  data.forEach(function(order){
                      past_t_minus_one_order.push(order);
                  });
                  /*for(var i = 0; i< till_now_order.length ; i++){
                      if(till_now_order[i]['booking_time']>=past_one_hour_time){
                          hour_order.push(till_now_order[i]);
                      } 
                      if(till_now_order[i]['booking_time'] >= today_midnight){
                          past_one_day_order.push(till_now_order[i]);
                      }
                  }*/
                  callback(err,data); 
              }
          });
      }
      ],function(err,response){
            if(err){
                console.log('Something broken',err);
                process.exit();
            }
            else{
                var data = {};
                data['one_day'] = collectInformation(past_t_minus_one_order,'W');
                var day_system_data = filter_system_wise_data(past_t_minus_one_order);
                data['one_day']['app'] = weak_system_data['app'];
                data['one_day']['web'] = weak_system_data['web'];
                data['weak'] = collectInformation(past_t_minus_seven_order,'W');
                var weak_system_data = filter_system_wise_data(past_t_minus_seven_order);
                data['weak']['app'] = weak_system_data['app'];
                data['weak']['web'] = weak_system_data['web'];
                data ['today']= collectInformation(past_one_day_order,'T');
                var today_system_data = filter_system_wise_data(past_one_day_order);
                data['today']['app'] = today_system_data['app'];
                data['today']['web'] = today_system_data['web'];
                data['hour'] = collectInformation(hour_order,'H');
                var hour_system_data = filter_system_wise_data(hour_order);
                data['hour']['app'] = hour_system_data['app'];
                data['hour']['web'] = hour_system_data['web'];
                var date=  new Date();
                date = JSON.stringify(date);
                var html_text = Template.generateTemplate(data['today']);
                        console.log(html_text);
                        mail.send_report_mail(html_text,date,2, function(error, response) { 
                            if (error) {
                                   console.log('Failed to send mail...');
                            }
                            else{
                                console.log('succesfully send mail');
                            }
                         process.exit();
                       });

                async.parallel_request(data,function(err,resp){
                    if(err) console.log('error is getting when calling function',err);
                    else{
                      console.log('succesfully fetch data from kibana');
                    }
                });

                /*bookingdb.parallel_request(data['today']['whole_data']['s'],function(err,resp){
                    if(err) console.log('error is getting when calling function',err);
                    else{
                      console.log('succesfully fetch data from booking db');
                    }
                });

                /*var html_text = Template.generateTemplate1(data['hour']);
                        console.log(html_text);
                        mail.send_report_mail(html_text,date,2, function(error, response) { 
                            if (error) {
                                   console.log('Failed to send mail...');
                            }
                            else{
                                console.log('succesfully send mail');
                            }
                         process.exit();
                });*/
            }
      })
  }
  catch(error){
  	console.log('Some Database problem',error);
    process.exit();
  }
})();

function collectInformation(orders,type){
    var data = {};
    whole_data = {};

     /* last_hour_data_with_partner = {};
      pending_order_list_with_provider = [];
      flag =  1;
      pending_order_id_list = [];
      /*total_order = orders.length;
      total_tickets = 0,
      total_failure_order = 0,
      total_failure_tickets = 0,
      total_failed_gmv = 0,
      total_cancelation_request = 0,
      total_cashback = 0,
      precentage_cashback = 0,
      total_gmv = 0,
      total_success_order = 0,
      total_success_tickets=0,
      total_success_gmv=0,
      total_cancel_tickets=0,
      total_canceled_gmv=0,
      total_pending_tickets = 0,
      total_pending_gmv=0,
      cancel_order = 0,
      success_order = 0,
      success_gmv = 0,
      failed_order = 0,
      total_pending_order=0,
      total_oneway_success_order = 0;
      total_roundtrip_success_order = 0;
      total_oneway_tickets = 0;
      total_roundtrip_tickets = 0;
      total_oneway_failed_order = 0;
      total_roundtrip_failed_order = 0;
      total_oneway_failed_tickets = 0;
      total_roundtrip_failed_tickets = 0;
      total_oneway_success_tickets = 0;
      total_roundtrip_success_tickets = 0;*/
      success_gmv = 0;
      success_orders = 0;
      success_tickets = 0;
      ac_gmv = 0;
      ac_orders = 0;
      ac_tickets = 0;
      failed_gmv = 0;
      failed_orders = 0;
      failed_tickets = 0;
      /*waiting_gmv = 0;
      waiting_orders =0;
      waiting_tickets=0;*/
      waiting_flag = false;

    for(var i=0; i<orders.length ; i++){
        //total_tickets = total_tickets + orders[i]['order']['items'].length;
        //try{
        //var item = JSON.parse(orders[i].order.items[0].meta_data);}
        //catch(err){var item = orders[i].order.items[0].metaInfo.meta_data;}
        //console.log(item);
      try{
       // if(item.journey.seller == 'SastiTicket' && type== 'H'){  }
        if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS'
           && orders[i]['booking_status'] && orders[i]['booking_status']==='SUCCESS'){
          success_orders++;
          success_tickets= success_tickets + orders[i]['order']['items'].length;
          success_gmv = success_gmv + orders[i]['order']['grandtotal'];
          
          /8var query3 = {
             "order_id" : orders[i].order_id
          };
        function(callback){
          Booking.find(query3,function(err,data){
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  if(data[0].pax_info.booking_status!= 'CNF')
                     waiting.push(order);
                   }
                  });
                  callback(err,data); 
              }*/


          if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
            console.log(orders[i]['meta']['query']['class']);
            ac_orders++;
            ac_tickets = ac_tickets + orders[i]['order']['items'].length;
            ac_gmv = ac_gmv + orders[i]['order']['grandtotal'];
          }
        }
        if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS' 
           && orders[i]['booking_status'] && orders[i]['booking_status']==='FAILED'){
          failed_orders++;
          failed_tickets= failed_tickets + orders[i]['order']['items'].length;
          failed_gmv = failed_gmv + orders[i]['order']['grandtotal'];
        }
      } catch(err){
        console.log(orders[i].order_id);
        }

    }
    whole_data['success_gmv'] = success_gmv;
    whole_data['success_order'] = success_orders;
    whole_data['success_tickets'] = success_tickets;
    whole_data['ac_gmv'] = ac_gmv;
    whole_data['ac_orders'] = ac_orders;
    whole_data['ac_tickets'] = ac_tickets;
    whole_data['failed_gmv'] = failed_gmv;
    whole_data['failed_orders'] = failed_orders;
    whole_data['failed_tickets'] = failed_tickets;


    for(k=0;k<waiting.length;k++)
    {
      if()
    }
    whole_data['waiting_orders'] = waiting_orders;
    whole_data['waiting_gmv'] = waiting_gmv;
    whole_data['waiting_tickets'] =waiting_tickets;


    /*whole_data['orders_with_promo_applied'] = null;
    whole_data['cashback_amount'] = null;
    whole_data['cashback_percentage'] = null;
    whole_data['pending_order'] = total_pending_order;
    whole_data['total_roundtrip_success_orders'] = total_roundtrip_success_order;
    whole_data['total_oneway_success_order'] = total_oneway_success_order;
    whole_data['total_oneway_tickets'] = total_oneway_tickets;
    whole_data['total_roundtrip_tickets'] = total_roundtrip_tickets;
    whole_data['total_oneway_failed_order'] = total_oneway_failed_order;
    whole_data['total_roundtrip_failed_order'] = total_roundtrip_failed_order;
    whole_data['total_oneway_failed_tickets'] = total_oneway_failed_tickets;
    whole_data['total_roundtrip_failed_tickets'] = total_roundtrip_failed_tickets;
    whole_data['total_oneway_success_tickets'] = total_oneway_success_tickets;
    whole_data['total_roundtrip_success_tickets'] = total_roundtrip_success_tickets;*/

 
    /*last_hour_data_with_partner['ezeego'] = createJSON(ezeego_details);
    last_hour_data_with_partner['emt'] = createJSON(emt_details);
    last_hour_data_with_partner['pearl'] = createJSON(pearl_details);*/
    data['whole_data'] = whole_data;
    //data['last_hour_data_with_partner'] = last_hour_data_with_partner;
    //data['pending_order_list_with_provider'] = pending_order_list_with_provider;
    //console.log(data);
    return data;
}


/*function createJSON(provider_info){
    var data = {};
    data['failed_gmv'] = provider_info.failed_gmv;
    data['gmv'] = provider_info.gmv;
    data['orders'] = provider_info.orders;
    data['tickets'] = provider_info.tickets;
    data['failed_order'] = provider_info.failed_orders;
    data['failed_tickets'] = provider_info.failed_tickets;
    data['cashback_percentage'] = null;
    data['pending_order'] = provider_info.pending_order;
    data['failed_gmv'] = provider_info.failed_gmv;
    data['roundtrip_success_orders'] = provider_info.roundtrip_success_orders;
    data['oneway_success_orders'] = provider_info.oneway_success_orders;
    data['roundtrip_tickets'] = provider_info.roundtrip_tickets;
    data['oneway_tickets'] = provider_info.oneway_tickets;
    data['roundtrip_failed_orders'] = provider_info.roundtrip_failed_orders;
    data['oneway_failed_orders'] = provider_info.oneway_failed_orders;
    data['roundtrip_failed_tickets'] = provider_info.roundtrip_failed_tickets;
    data['roundtrip_success_tickets'] = provider_info.roundtrip_success_tickets;
    data['oneway_success_tickets'] = provider_info.oneway_success_tickets;
    data['oneway_failed_tickets'] = provider_info.oneway_failed_tickets;
    return data;
}*/

function filter_system_wise_data(orders){
    var data = {};
      app = {};
      web = {};
      success_gmv_app = 0;
      success_orders_app = 0;
      success_tickets_app = 0;
      ac_gmv_app = 0;
      ac_orders_app = 0;
      ac_tickets_app = 0;
      failed_gmv_app = 0;
      failed_orders_app = 0;
      failed_tickets_app = 0;
      success_gmv_web = 0;
      success_orders_web = 0;
      success_tickets_web = 0;
      ac_gmv_web = 0;
      ac_orders_web = 0;
      ac_tickets_web = 0;
      failed_gmv_web = 0;
      failed_orders_web = 0;
      failed_tickets_web = 0;
    for(var i=0; i<orders.length ; i++){
        try{
        //var item = JSON.parse(orders[i].meta.order.items[0].meta_data);
          if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS'
             && orders[i]['booking_status'] && orders[i]['booking_status']==='SUCCESS'){
              if((orders[i].order.channel_id).indexOf('ANDROID')>-1 || (orders[i].order.channel_id).indexOf('IOS')>-1){
                  success_orders_app++;
                  success_tickets_app= success_tickets_app + orders[i]['order']['items'].length;
                  success_gmv_app = success_gmv_app + orders[i]['order']['grandtotal'];
                  if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                    console.log(orders[i]['meta']['query']['class']);
                    ac_orders_app++;
                    ac_tickets_app = ac_tickets_app + orders[i]['order']['items'].length;
                    ac_gmv_app = ac_gmv_app + orders[i]['order']['grandtotal'];
                  }
              }
              else{
                  success_orders_web++;
                  success_tickets_web= success_tickets_web + orders[i]['order']['items'].length;
                  success_gmv_web = success_gmv_web + orders[i]['order']['grandtotal'];
                  if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                    console.log(orders[i]['meta']['query']['class']);
                    ac_orders_web++;
                    ac_tickets_web = ac_tickets_web + orders[i]['order']['items'].length;
                    ac_gmv_web = ac_gmv_web + orders[i]['order']['grandtotal'];
                  }

              }
          }if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS' 
           && orders[i]['booking_status'] && orders[i]['booking_status']==='FAILED'){
              if((orders[i].order.channel_id).indexOf('ANDROID')>-1 || (orders[i].order.channel_id).indexOf('IOS')>-1){
                  failed_orders_app++;
                  failed_tickets_app= failed_tickets_app + orders[i]['order']['items'].length;
                  failed_gmv_app = failed_gmv_app + orders[i]['order']['grandtotal'];
                  /*if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                    console.log(orders[i]['meta']['query']['class']);
                    ac_orders_app++;
                    ac_tickets_app = ac_tickets_app + orders[i]['order']['items'].length;
                    ac_gmv_app = ac_gmv_app + orders[i]['order']['grandtotal'];
                  }*/
              }
              else{
                  failed_orders_web++;
                  failed_tickets_web= failed_tickets_web + orders[i]['order']['items'].length;
                  failed_gmv_web = failed_gmv_web + orders[i]['order']['grandtotal'];
                  /*if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                    console.log(orders[i]['meta']['query']['class']);
                    ac_orders_web++;
                    ac_tickets_web = ac_tickets_web + orders[i]['order']['items'].length;
                    ac_gmv_web = ac_gmv_web + orders[i]['order']['grandtotal'];
                  }*/

              }
            }
        } catch(err){}
    }


    app['success_gmv'] = success_gmv_app;
    app['success_order'] =success_orders_app;
    app['success_tickets'] = success_tickets_app;
    app['ac_gmv'] = ac_gmv_app;
    app['ac_orders'] = ac_orders_app;
    app['ac_tickets'] = ac_tickets_app;
    app['failed_gmv'] =failed_gmv_app;
    app['failed_orders'] =failed_orders_app;
    app['failed_tickets'] = failed_tickets_app;

    web['success_gmv'] = success_gmv_web;
    web['success_order'] =success_orders_web;
    web['success_tickets'] = success_tickets_web;
    web['ac_gmv'] = ac_gmv_web;
    web['ac_orders'] = ac_orders_web;
    web['ac_tickets'] = ac_tickets_web;
    web['failed_gmv'] =failed_gmv_web;
    web['failed_orders'] =failed_orders_web;
    web['failed_tickets'] = failed_tickets_web;


//    console.log('System Data is',app_details);
    data['app'] = app;
    data['web'] = web;
//    console.log('System Data is',data);
    return data; 
}

/*function createSystemJSON(system_info){
    var data = {};
    data['failed_gmv'] = system_info.failed_gmv;
    data['gmv'] = system_info.gmv;
    data['orders'] = system_info.order;
    data['tickets'] = system_info.tickets;
    data['failed_order'] = system_info.failed_order;
    data['failed_tickets'] = system_info.failure_tickets;
    data['cashback_percentage'] = null;
    data['pending_order'] = system_info.pending_order;
    data['roundtrip_success_orders'] = system_info.roundtrip_success_order;
    data['oneway_success_orders'] = system_info.oneway_success_order;
    data['roundtrip_tickets'] = system_info.roundtrip_tickets;
    data['oneway_tickets'] = system_info.oneway_tickets;
    data['roundtrip_failed_orders'] = system_info.roundtrip_failed_order;
    data['oneway_failed_orders'] = system_info.oneway_failed_order;
    data['roundtrip_failed_tickets'] = system_info.roundtrip_failed_tickets;
    data['roundtrip_success_tickets'] = system_info.roundtrip_success_tickets;
    data['oneway_success_tickets'] = system_info.oneway_success_tickets;
    data['oneway_failed_tickets'] = system_info.oneway_failed_tickets;
    return data;
}*/

