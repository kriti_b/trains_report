var async = require('async');
var request = require('request');
var dateFormat = require('dateformat');
var btoa = require('btoa');
var moment = require('moment');
var mongodb = require('mongodb');
var config = require('/home/ubuntu/src/paytm-travel/src/scripts/mongo/hourly/config/config.js');
var Template = require(global.__base + 'scripts/mongo/Trains_alerts/src/template_trains.js');
var mail = require(global.__base + 'scripts/mongo/Trains_alerts/src/send_mail_report.js');
var currentHour = moment().utc().format('YYYY-MM-DD HH:mm:ss');
var pastQuarterHour = moment().utc().subtract(30,'minutes').format('YYYY-MM-DD HH:mm:ss');
currentHour = moment(currentHour).valueOf();
pastQuarterHour = moment(pastQuarterHour).valueOf();
var datelogstash = "trains-" + moment().format('YYYY.MM.DD');
var yesterdaydatelogstash = "trains-" + moment().add(-1, 'days').format('YYYY.MM.DD');
var query1 = {
   "query": {
      "query_string": {
         "query": "\"Error while cart checkout\""
      }
   }
};

var query2 = {
   "query": {
      "query_string": {
         "query": "\"Request received to cancel Booking\""
      }
   }
}

var query3 = {
   "query": {
      "query_string": {
         "query": "\"Error while cart checkout\"",
         "analyze_wildcard": false
      }
    },
    "filter": {
		"bool": {
			"must": [{
				"range": {
					"@timestamp": {
						"gte": pastQuarterHour,
						"lte": currentHour,
						"format": "epoch_millis"
					}
				}
			}],
			"must_not": []
		}
	}
};

var query4 = {
   "query": {
      "query_string": {
         "query": "\"Request received to cancel Booking\"",
         "analyze_wildcard": false
      }
    },
    "filter": {
		"bool": {
			"must": [{
				"range": {
					"@timestamp": {
						"gte": pastQuarterHour,
						"lte": currentHour,
						"format": "epoch_millis"
					}
				}
			}],
			"must_not": []
		}
	}
};

var query5 = {
   "query": {
      "query_string": {
         "query": "\"Successfully cart checkout\""
      }
   }
};

var query6 = {
   "query": {
      "query_string": {
         "query": "\"Successfully cart checkout\"",
         "analyze_wildcard": false
      }
    },
    "filter": {
		"bool": {
			"must": [{
				"range": {
					"@timestamp": {
						"gte": pastQuarterHour,
						"lte": currentHour,
						"format": "epoch_millis"
					}
				}
			}],
			"must_not": []
		}
	}
};
var metricsUrl = config.options.url + datelogstash + '/_search';
var headers = config.options.headers;
headers.Authorization += btoa(config.username + ':' + config.password);
console.log(query1);
console.log(metricsUrl);
//var async_data = {};
//async_data['hour'] = {};
//async_data['today'] = {};
//async_data['weak'] = {};
//async_data['one_day'] ={};
exports.parallel_request = function(async_data){
	async.parallel([
		function(callback){
			request({
				method:'POST',
				url: metricsUrl,
				headers: headers,
				body: JSON.stringify(query5)
			    },
				function( error, response, body){
					if(!error && response){
						var resp = JSON.parse(body);
						async_data['today']['whole_data']['successful_check_outs'] = resp.hits.total;
						callback(null, async_data);
					}
					else{
						console.log("TOTEL SEARCHES ERROR");
						console.log(error);
						callback('totalSearchesError');
					}
				}
			)
		},
		function(callback){
			request({
				method:'POST',
				url: metricsUrl,
				headers: headers,
				body: JSON.stringify(query2)
				},
				function( error, response, body){
					if(!error && response){
						var resp = JSON.parse(body);
                                                console.log(resp);
						async_data['today']['whole_data']['cancellation_requests_tickets'] = resp.hits.total;
						callback(null, async_data);
					}
					else{
						console.log("TOTEL SEARCHES ERROR");
						console.log(error);
						callback('totalSearchesError');
					}
				}
			)
		},
		function(callback){
			request({
				method:'POST',
				url: metricsUrl,
				headers: headers,
				body: JSON.stringify(query6)
				},
				function( error, response, body){
					if(!error && response){
						var resp = JSON.parse(body);
						async_data['hour']['whole_data']['successful_check_outs'] = resp.hits.total;
						callback(null, async_data);
					}
					else{
						console.log("TOTEL SEARCHES ERROR");
						console.log(error);
						callback('totalSearchesError');
					}
				}
			)
		},
		function(callback){
			request({
				method:'POST',
				url: metricsUrl,
				headers: headers,
				body: JSON.stringify(query4)
				},
				function( error, response, body){
					if(!error && response){
						var resp = JSON.parse(body);
						async_data['hour']['whole_data']['cancellation_requests_tickets'] = resp.hits.total;
						callback(null, async_data);
					}
					else{
						console.log("TOTEL SEARCHES ERROR");
						console.log(error);
						callback('totalSearchesError');
					}
				}
			)
		},
		function(callback){
			request({
				method:'POST',
				url: metricsUrl,
				headers: headers,
				body: JSON.stringify(query1)
				},
				function( error, response, body){
					if(!error && response){
						var resp = JSON.parse(body);
						async_data['today']['whole_data']['failed_check_outs'] = resp.hits.total;
						callback(null, async_data);
					}
					else{
						console.log("TOTEL SEARCHES ERROR");
						console.log(error);
						callback('totalSearchesError');
					}
				}
			)
		},
		function(callback){
			request({
				method:'POST',
				url: metricsUrl,
				headers: headers,
				body: JSON.stringify(query3)
				},
				function( error, response, body){
					if(!error && response){
						var resp = JSON.parse(body);
						async_data['hour']['whole_data']['failed_check_outs'] = resp.hits.total;
						callback(null, async_data);
					}
					else{
						console.log("TOTEL SEARCHES ERROR");
						console.log(error);
						callback('totalSearchesError');
					}
				}
			)
		}
	],function(err,resp){
		if(err){
			console.log('error is getting');
                        process.exit();
		}
		else{
			console.log('suucessfully fetch data from kibana');
			console.log(async_data);
                        var date = new Date();
                        var from = date;
                        from.setHours(from.getHours()+5);
                        from.setMinutes(from.getMinutes()+30);
                        var minute = from.getMinutes();
                        var hour = from.getHours();
                        if(hour == 0) hour = 24;
                        hour = hour.toString();
                        if(hour.length == 1) hour = '0' + hour;
                        minute = minute.toString();
                        if(minute.length == 1) minute = '0' + minute; 
                        from = hour + ':' + minute;
                        var to = new Date();
                        to.setHours(to.getHours()+5);
                        minute = to.getMinutes();
                        hour = to.getHours();
                        //console.log(minute);
                        if(hour == 0) hour = 24;
                        hour = hour.toString();
                        if(hour.length == 1) hour = '0' + hour;
                        minute = minute.toString();
                        console.log('Minutess is',minute.length);
                        if(minute.length == 1) minute = '0' + minute;
                        to = hour + ':' + minute;
                        var date = new Date(); date.setHours(date.getHours()+5); date.setMinutes(date.getMinutes()+30);
                        date = dateFormat(date, "dS mmmm, yyyy");
                        var date1 = new Date();
                        var html_text = Template.generateTemplate1(async_data['hour'],async_data['today'],async_data['weak'],async_data['one_day']);
                        var  html_text_ac =  Template.generateTemplate2(async_data['hour'],async_data['today'],async_data['weak'],async_data['one_day']);
                        console.log(html_text);
                        console.log(html_text_ac);
                        mail.send_report_mail2(html_text,html_text_ac,date1,2, function(error, response) { 
                            if (error) {
                                   console.log('Failed to send mail...');
                            }
                            else{
                                console.log('succesfully send mail');
                            }
                         process.exit();
						});
                       /* mail.send_report_mail(html_text_ac,date,2, function(error, response) { 
                            if (error) {
                                   console.log('Failed to send mail...');
                            }
                            else{
                                console.log('succesfully send mail');
                            }
                         process.exit();
						});*/
		}
	});

}
