var request = require('request');
var btoa = require('btoa');
var async1 = require('async');
var mongoose = require('mongoose');
var fs = require('fs');
var parse = require('csv-parse');
var dateFormat = require('dateformat');
var CronJob = require('cron').CronJob;
global.__base= __dirname.split('scripts')[0];
var Transaction = require(global.__base + 'data/models/trains_db/transaction.js');
var Booking = require(global.__base + 'data/models/trains_db/booking.js');
var config = require(global.__base + 'scripts/mongo/Trains_alerts/src/config.js');
var mail = require(global.__base + 'scripts/mongo/Trains_alerts/src/send_mail_report.js');
var Template = require(global.__base + 'scripts/mongo/Trains_alerts/src/template_trains.js');
var async = require(global.__base + 'scripts/mongo/Trains_alerts/src/elasticsearch/asynctask1.js');
//var bookingdb = require(global.__base + 'scripts/mongo/Trains_alerts/src/trains_booking.js');
function ConString(){
	var index = 0;
	var connection_string = "mongodb://";

	for(index = 1; index <config.db.mongodb.servers.length; ++index) {
		connection_string += config.db.mongodb.servers[index].host 
						+ ":" + config.db.mongodb.servers[index].port;
		connection_string += ',';
	}
	connection_string = connection_string.slice(0, -1);
	connection_string += '/';
	connection_string += config.db.mongodb.db_train_name + '?replicaSet=' + config.db.mongodb.replica_set;
	return connection_string;
}


mongoose.connect(ConString(), config.db.mongodb.db_options,function(err){
});
(function generateDailyReportInTrain(){
  var data = {};
  var past_one_hour_time = new Date();
  past_one_hour_time.setMinutes(past_one_hour_time.getMinutes()-30);
  var past_one_weak = new Date();
  past_one_weak.setHours(0,0,0,0);
  past_one_weak.setDate(past_one_weak.getDate()-7);
  past_one_weak.setHours(past_one_weak.getHours() - 5);
  past_one_weak.setMinutes(past_one_weak.getMinutes() - 30);
  var past_six_day = new Date();
  past_six_day.setDate(past_six_day.getDate()-7);
  var past_six_day_mid = new Date();
  past_six_day_mid.setHours(0,0,0,0);
  past_six_day_mid.setDate(past_six_day_mid.getDate()-6);
  past_six_day_mid.setHours(past_six_day_mid.getHours()-5);
  past_six_day_mid.setMinutes(past_six_day_mid.getMinutes()-30);
  var past_one_day = new Date();
  past_one_day.setHours(0,0,0,0);
  past_one_day.setDate(past_one_weak.getDate()-1);
  past_one_day.setHours(past_one_weak.getHours() - 5);
  past_one_day.setMinutes(past_one_weak.getMinutes() - 30);
  var past_zero_day = new Date();
  past_zero_day.setDate(past_six_day.getDate()-1);
  var past_one_day_new = new Date();
  past_one_day_new.setHours(0,0,0,0);
  past_one_day_new.setDate(past_one_day_new.getDate()-1);
  past_one_day_new.setHours(past_one_day_new.getHours() - 5);
  past_one_day_new.setMinutes(past_one_day_new.getMinutes() - 30);
  var past_zero_day_new = new Date();
  past_zero_day_new.setDate(past_zero_day_new.getDate()-1);
 // var past_one_day_new = new Date(); 
  var today_midnight = new Date();
  today_midnight.setHours(0,0,0,0);
  today_midnight.setHours(today_midnight.getHours() - 5);
  today_midnight.setMinutes(today_midnight.getMinutes() - 30);
  var tomorrow_midnight = new Date();
  tomorrow_midnight.setHours(0,0,0,0);
  tomorrow_midnight.setHours(tomorrow_midnight.getHours()+18);
  tomorrow_midnight.setMinutes(tomorrow_midnight.getMinutes()+30);
 // tomorrow_midnight.setHours(tomorrow_midnight.getHours() - 5);
 // tomorrow_midnight.setMinutes(tomorrow_midnight.getMinutes() - 30);
 // tomorrow_midnight.setDate(tomorrow_midnight.getDate()+1);
  var current_time = new Date();
  current_time.setHours(current_time.getHours());
  current_time.setMinutes(current_time.getMinutes());
  var hour = current_time.getHours();
  var past_one_hour = hour -1;
  var date = current_time.toDateString();
  console.log("midnigt",tomorrow_midnight);
  console.log("last six day",past_six_day_mid);
  var query = {
      "booking_time" : {
          "$gte" : today_midnight,
          "$lte" : current_time
      }
  };
  var query1 = {
      "booking_time" : {
          "$gte" : past_one_weak,
          "$lte" : past_six_day
      }
  };

  var query2 = {
      "booking_time" : {
          "$gte" : past_one_day,
          "$lte" : past_zero_day
      }
  };

  var query2_new = {
      "booking_time" : {
          "$gte" : past_one_day_new,
          "$lte" : past_zero_day_new
      }
  };


  var query3 = {
      "created_at" : {
          "$gte" : today_midnight,
          "$lte" : current_time
      }
  };

  var query4 = {
      "booking_time" : {
          "$gte" : past_one_day_new,
          "$lte" : today_midnight

      }
  };
  

  var query5 = {
      "created_at" : {
          "$gte" : past_one_day_new,
          "$lte" : today_midnight

      }
  };



  
  var hour_order =[];
  var html_text = "";
  var till_now_order = [];
  var past_one_day_order = [];
  var past_t_minus_seven_order = [];
  var past_t_minus_seven_order_new = [];
  var past_t_minus_seven_hour_order = [];
  var past_t_minus_one_order = [];
  var past_t_minus_one_order_new = [];
  var confirm_status = [];
  var confirm_status_new = [];
  var confirm_status_past_one_day_order = [];
  var confirm_status_past_one_day_order_new = [];
  //console.log('******************************',past_one_hour_time);
  try{
    async1.parallel([
      function(callback){
          Transaction.find(query1,function(err,data){
              console.log(query1,"query1");
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  data.forEach(function(order){
                      past_t_minus_seven_order.push(order);
                    /* for(var i = 0; i< past_t_minus_seven_order.length ; i++){
                     /* if(till_now_order[i]['booking_time']>=past_one_hour_time){
                          hour_order.push(till_now_order[i]);
                      }*/
                     /* if(past_t_minus_seven_order[i]['booking_time'] <=past_six_day_mid){
                          past_t_minus_seven_order_new.push(past_t_minus_seven_order[i]);
                      }
                  }*/
                      //if(order.booking_time >= t_past_hour){
                        //   past_t_minus_seven_hour_order.push(order);
                     // }
                  });
                  callback(err,data); 
              }
          });
      },
      function(callback){
          Transaction.find(query,function(err,data){
              console.log(query,"query");
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  data.forEach(function(order){
                      till_now_order.push(order);
                  });
                  for(var i = 0; i< till_now_order.length ; i++){
                      if(till_now_order[i]['booking_time']>=past_one_hour_time){
                          hour_order.push(till_now_order[i]);
                      } 
                      if(till_now_order[i]['booking_time'] >= today_midnight){
                          past_one_day_order.push(till_now_order[i]);
                      }
                  }
                  callback(err,data); 
              }
          });
      },
      function(callback){
          Transaction.find(query2_new,function(err,data){
              console.log(query2_new,"query2");
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  data.forEach(function(order){
                      past_t_minus_one_order.push(order);
                  });
                  /*for(var i = 0; i< till_now_order.length ; i++){
                      if(till_now_order[i]['booking_time']>=past_one_hour_time){
                          hour_order.push(till_now_order[i]);
                      } 
                      if(till_now_order[i]['booking_time'] >= today_midnight){
                          past_one_day_order.push(till_now_order[i]);
                      }
                  }*/
                  callback(err,data); 
              }
          });
      },
      function(callback){
          Transaction.find(query4,function(err,data){
              console.log(query4,"query4");
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  data.forEach(function(order){
                      past_t_minus_one_order_new.push(order);
                  });
                  /*for(var i = 0; i< till_now_order.length ; i++){
                      if(till_now_order[i]['booking_time']>=past_one_hour_time){
                          hour_order.push(till_now_order[i]);
                      } 
                      if(till_now_order[i]['booking_time'] >= today_midnight){
                          past_one_day_order.push(till_now_order[i]);
                      }
                  }*/
                  callback(err,data);
              }
          });
      },
      function(callback){
          Booking.find(query5,function(err,data){
              console.log(query5,"query5");
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  data.forEach(function(order){
                      confirm_status_new.push(order);
                  });
                  for(var i = 0; i< confirm_status_new.length ; i++){
                      /*if(till_now_order[i]['booking_time']>=past_one_hour_time){
                          hour_order.push(till_now_order[i]);
                      } 
                      if(confirm_status_new[i]['created_at'] <= today_midnight){
                          confirm_status_past_one_day_order_new.push(confirm_status_new[i]);
                      }*/
                  }
                  callback(err,data);
              }
          });
      },


      function(callback){
          Booking.find(query3,function(err,data){
              console.log(query3,"query3");
              if(err || !data){
                  console.log("no data present in collection");
                  callback(err,data);
              }else{
                  data.forEach(function(order){
                      confirm_status.push(order);
                  });
                  for(var i = 0; i< confirm_status.length ; i++){
                      /*if(till_now_order[i]['booking_time']>=past_one_hour_time){
                          hour_order.push(till_now_order[i]);
                      } */
                      if(confirm_status[i]['created_at'] >= today_midnight){
                          confirm_status_past_one_day_order.push(confirm_status[i]);
                      }
                  }
                  callback(err,data); 
              }
          });
      }
      ],function(err,response){
            if(err){
                console.log('Something broken',err);
                process.exit();
            }
            else{
                var data = {};
                data['one_day'] = collectInformation(past_t_minus_one_order,'W');
                var day_system_data = filter_system_wise_data(past_t_minus_one_order);
                data['one_day']['app'] = day_system_data['app'];
                data['one_day']['web'] = day_system_data['web'];
                data['last_day'] = collectInformation(past_t_minus_one_order_new);
                var day_ac_data = filter_ac_wise_data(past_t_minus_one_order);
                data['one_day']['ac'] = day_ac_data['ac'];
                data['one_day']['non_ac'] = day_ac_data['non_ac'];
                data['weak'] = collectInformation(past_t_minus_seven_order,'W');
                var weak_system_data = filter_system_wise_data(past_t_minus_seven_order);
                data['weak']['app'] = weak_system_data['app'];
                data['weak']['web'] = weak_system_data['web'];
                var weak_ac_data = filter_ac_wise_data(past_t_minus_seven_order);
                data['weak']['ac'] = weak_ac_data['ac'];
                data['weak']['non_ac'] =weak_ac_data['non_ac'];
                data ['today']= collectInformation(past_one_day_order,'T');
                var today_system_data = filter_system_wise_data(past_one_day_order);
                data['today']['app'] = today_system_data['app'];
                data['today']['web'] = today_system_data['web'];
                var today_ac_data = filter_ac_wise_data(past_one_day_order);
                data['today']['ac'] = today_ac_data['ac'];
                data['today']['non_ac'] = today_ac_data['non_ac'];
                data['hour'] = collectInformation(hour_order,'H');
                var hour_system_data = filter_system_wise_data(hour_order);
                data['hour']['app'] = hour_system_data['app'];
                data['hour']['web'] = hour_system_data['web'];
                var hour_ac_data = filter_ac_wise_data(hour_order);
                data['hour']['ac'] = hour_ac_data['ac'];
                data['hour']['non_ac'] = hour_ac_data['non_ac'];
               // data['status'] = collectstatus(confirm_status_past_one_day_order);
               // data['status_new'] = collectstatus(confirm_status_new);
                var date=  new Date();
                date = JSON.stringify(date);
                var html_text = Template.generateTemplate(data['today']);
                var html_text_class = Template.generateTemplateClass(data['today']);
               // var html_text_status = Template.generateTemplatestatus(data['status']);
                  var html_text_status = Template.generateTemplatestatus(data['today']);
                      console.log(html_text);
                      var date = new Date();
                      var current_hour = date.getHours();
                      var current_min = date.getMinutes();
                      console.log("hour",current_hour);
                       if(current_hour==18){
                        mail.send_report_mail3(html_text,html_text_class,html_text_status,date,2, function(error, response) { 
                            if (error) {
                                   console.log('Failed to send mail...');
                            }
                            else{
                                console.log('succesfully send mail');
                            }
                         process.exit();
                       });
                       }
                     /* var html_text = Template.generateTemplate(data['last_day']);
                      var html_text_class = Template.generateTemplateClass(data['last_day']);
                      var html_text_status = Template.generateTemplatestatus(data['status_new']);
                      //console.log(html_text);
                      //var date = new Date();
                      //var current_hour = date.getHours();
                      //var current_min = date.getMinutes();
                      //console.log("hour",current_hour);
                      // if(current_hour==3){
                        mail.send_report_mail3(html_text,html_text_class,html_text_status,date,2, function(error, response) {
                            if (error) {
                                   console.log('Failed to send mail...');
                            }
                            else{
                                console.log('succesfully send mail');
                            }
                         process.exit();
                       });
                     // }
 
               /* var html_text_class = Template.generateTemplateClass(data['today']);
                       // console.log(html_text);
                        mail.send_report_mail(html_text_class,date,2, function(error, response) { 
                            if (error) {
                                   console.log('Failed to send mail...');
                            }
                            else{
                                console.log('succesfully send mail');
                            }
                         process.exit();
                       });

                var html_text_status = Template.generateTemplatestatus(data['status']);
                        console.log(html_text_status);
                        mail.send_report_mail(html_text_status,date,2, function(error, response) { 
                            if (error) {
                                   console.log('Failed to send mail...');
                            }
                            else{
                                console.log('succesfully send mail');
                            }
                         process.exit();
                       });*/

                async.parallel_request(data,function(err,resp){
                    if(err) console.log('error is getting when calling function',err);
                    else{
                      console.log('succesfully fetch data from kibana');
                    }
                });

                /*bookingdb.parallel_request(data['today']['whole_data']['s'],function(err,resp){
                    if(err) console.log('error is getting when calling function',err);
                    else{
                      console.log('succesfully fetch data from booking db');
                    }
                });

                /*var html_text = Template.generateTemplate1(data['hour']);
                        console.log(html_text);
                        mail.send_report_mail(html_text,date,2, function(error, response) { 
                            if (error) {
                                   console.log('Failed to send mail...');
                            }
                            else{
                                console.log('succesfully send mail');
                            }
                         process.exit();
                });*/
            }
      })
  }
  catch(error){
  	console.log('Some Database problem',error);
    process.exit();
  }
})();

function collectInformation(orders,type){
  
    var data = {};

    
        whole_data = {};

     /* last_hour_data_with_partner = {};
      pending_order_list_with_provider = [];
      flag =  1;
      pending_order_id_list = [];
      /*total_order = orders.length;
      total_tickets = 0,
      total_failure_order = 0,
      total_failure_tickets = 0,
      total_failed_gmv = 0,
      total_cancelation_request = 0,
      total_cashback = 0,
      precentage_cashback = 0,
      total_gmv = 0,
      total_success_order = 0,
      total_success_tickets=0,
      total_success_gmv=0,
      total_cancel_tickets=0,
      total_canceled_gmv=0,
      total_pending_tickets = 0,
      total_pending_gmv=0,
      cancel_order = 0,
      success_order = 0,
      success_gmv = 0,
      failed_order = 0,
      total_pending_order=0,
      total_oneway_success_order = 0;
      total_roundtrip_success_order = 0;
      total_oneway_tickets = 0;
      total_roundtrip_tickets = 0;
      total_oneway_failed_order = 0;
      total_roundtrip_failed_order = 0;
      total_oneway_failed_tickets = 0;
      total_roundtrip_failed_tickets = 0;
      total_oneway_success_tickets = 0;
      total_roundtrip_success_tickets = 0;*/
      success_gmv = 0;
      success_orders = 0;
      success_tickets = 0;
      ac_gmv = 0;
      ac_orders = 0;
      ac_tickets = 0;
      failed_gmv = 0;
      failed_orders = 0;
      failed_tickets = 0;
      cancelled_gmv =0;
      cancelled_orders =0;
      cancelled_tickets = 0;
      pending_gmv = 0;
      pending_orders = 0;
      pending_tickets = 0;

      fc_ac_orders = 0 ;
      ec_orders = 0 ;
      ac_2_orders = 0 ;
      fc_orders = 0;
      ac_3_orders = 0 ;
      ac_3_e_orders = 0;
      ac_chair_orders = 0 ;
      sl_orders = 0;
      ss_orders = 0;
      

      cnf_orders = 0;
      ckwl_orders = 0;
      rqwl_orders = 0;
      rac_orders = 0;
      rlwl_orders = 0;
      pqwl_orders = 0;
      wl_orders = 0;
      gnwl_orders = 0;

      /*waiting_gmv = 0;
      waiting_orders =0;
      waiting_tickets=0;*/
      //waiting_flag = false;

    for(var i=0; i<orders.length ; i++){
        //total_tickets = total_tickets + orders[i]['order']['items'].length;
        //try{
        //var item = JSON.parse(orders[i].order.items[0].meta_data);}
        //catch(err){var item = orders[i].order.items[0].metaInfo.meta_data;}
        //console.log(item);
      try{
       // if(item.journey.seller == 'SastiTicket' && type== 'H'){  }
        if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS'
           && orders[i]['booking_status'] && (orders[i]['booking_status']==='SUCCESS'|| orders[i]['booking_status']==='CANCEL_REQUESTED')){
          success_orders++;
          success_tickets= success_tickets + orders[i]['order']['items'].length;
          success_gmv = success_gmv + orders[i]['order']['grandtotal'];
          
          if(orders[i]['meta']['query']['class']==='1A'){
            fc_ac_orders = fc_ac_orders + orders[i]['order']['items'].length;
          }
          if(orders[i]['meta']['query']['class']==='EC'){
            ec_orders = ec_orders + orders[i]['order']['items'].length;
          }
          if(orders[i]['meta']['query']['class']==='2A'){
            ac_2_orders = ac_2_orders + orders[i]['order']['items'].length;
          }
          if(orders[i]['meta']['query']['class']==='FC'){
            fc_orders = fc_orders + orders[i]['order']['items'].length;
          }
          if(orders[i]['meta']['query']['class']==='3A'){
            ac_3_orders = ac_3_orders + orders[i]['order']['items'].length;
          }
          if(orders[i]['meta']['query']['class']==='3E'){
            ac_3_e_orders = ac_3_e_orders + orders[i]['order']['items'].length;
          }
          if(orders[i]['meta']['query']['class']==='CC'){
            ac_chair_orders = ac_chair_orders + orders[i]['order']['items'].length;
          }
          if(orders[i]['meta']['query']['class']==='SL'){
            sl_orders = sl_orders + orders[i]['order']['items'].length;
          }
          if(orders[i]['meta']['query']['class']==='2S'){
            ss_orders = ss_orders + orders[i]['order']['items'].length;
          }
          if(orders[i]['confirm_data']['psgnDtlList'][0]['currentStatus']==='CNF')
            cnf_orders = cnf_orders + orders[i]['order']['items'].length;

          if(orders[i]['confirm_data']['psgnDtlList'][0]['currentStatus']==='CKWL')
            ckwl_orders = ckwl_orders + orders[i]['order']['items'].length;

          if(orders[i]['confirm_data']['psgnDtlList'][0]['currentStatus']==='PQWL')
            pqwl_orders = pqwl_orders + orders[i]['order']['items'].length;

          if(orders[i]['confirm_data']['psgnDtlList'][0]['currentStatus']==='RLWL')
            rlwl_orders = rlwl_orders + orders[i]['order']['items'].length;
          
         if(orders[i]['confirm_data']['psgnDtlList'][0]['currentStatus']==='RAC')
            rac_orders = rac_orders + orders[i]['order']['items'].length;

         if(orders[i]['confirm_data']['psgnDtlList'][0]['currentStatus']==='WL')
            wl_orders = wl_orders + orders[i]['order']['items'].length;

         if(orders[i]['confirm_data']['psgnDtlList'][0]['currentStatus']==='GNWL')
            gnwl_orders = gnwl_orders + orders[i]['order']['items'].length;
          
          if(orders[i]['confirm_data']['psgnDtlList'][0]['currentStatus']==='RQWL')
            rqwl_orders = rqwl_orders + orders[i]['order']['items'].length;
          

          if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
           // console.log(orders[i]['meta']['query']['class']);
            ac_orders++;
            ac_tickets = ac_tickets + orders[i]['order']['items'].length;
            ac_gmv = ac_gmv + orders[i]['order']['grandtotal'];
          }
        }
        if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS' 
           && orders[i]['booking_status'] && orders[i]['booking_status']==='FAILED'){
          failed_orders++;
          failed_tickets= failed_tickets + orders[i]['order']['items'].length;
          failed_gmv = failed_gmv + orders[i]['order']['grandtotal'];
        }
        if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS' 
           && orders[i]['booking_status'] && orders[i]['booking_status']==='CANCELLED'){
          cancelled_orders++;
          cancelled_tickets= cancelled_tickets + orders[i]['order']['items'].length;
          cancelled_gmv = cancelled_gmv + orders[i]['order']['grandtotal'];
        }
        if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS'
           && orders[i]['booking_status'] && orders[i]['booking_status']==='INITIATED'){
          pending_orders++;
          pending_tickets= pending_tickets + orders[i]['order']['items'].length;
          pending_gmv = pending_gmv + orders[i]['order']['grandtotal'];
        }

      } catch(err){
      //  console.log(orders[i].order_id);
        }

    }
    whole_data['success_gmv'] = success_gmv;
    whole_data['success_order'] = success_orders;
    whole_data['success_tickets'] = success_tickets;
    whole_data['ac_gmv'] = ac_gmv;
    whole_data['ac_orders'] = ac_orders;
    whole_data['ac_tickets'] = ac_tickets;
    whole_data['failed_gmv'] = failed_gmv;
    whole_data['failed_orders'] = failed_orders;
    whole_data['failed_tickets'] = failed_tickets;
    whole_data['cancelled_gmv'] =cancelled_gmv;
    whole_data['cancelled_orders'] = cancelled_orders;
    whole_data['cancelled_tickets'] = cancelled_tickets;
    whole_data['pending_gmv'] = pending_gmv;
    whole_data['pending_orders'] = pending_orders;
    whole_data['pending_tickets'] = pending_tickets;
    whole_data['1A'] = fc_ac_orders;
    whole_data['EC'] = ec_orders;
    whole_data['2A'] = ac_2_orders;
    whole_data['FC'] = fc_orders;
    whole_data['3A'] = ac_3_orders;
    whole_data['3E'] = ac_3_e_orders;
    whole_data['CC'] = ac_chair_orders;
    whole_data['SL'] = sl_orders;
    whole_data['2S'] = ss_orders;
    whole_data['CNF'] = cnf_orders;
    whole_data['CKWL'] = ckwl_orders;
    whole_data['RAC'] = rac_orders;
    whole_data['PQWL'] = pqwl_orders;
    whole_data['WL'] = wl_orders;
    whole_data['GNWL'] = gnwl_orders;
    whole_data['RQWL'] = rqwl_orders;
    whole_data['RLWL'] = rlwl_orders;


   /* var sortTicketsClass = [fc_ac_orders , ec_orders ,ac_2_orders ,fc_orders ,ac_3_orders ,ac_3_e_orders , ac_chair_orders,sl_orders,
     ss_orders];
     sortTicketsClass.sort(function(a, b) {
       return b-a;
     });   
    // sortTicketsClass.sort();
     for(var i=0;i<sortTicketsClass.length;i++)
      console.log(sortTicketsClass[i]," " );*/
    

    var sortClass = [
  { name: 'First Class AC(1A)', value: fc_ac_orders },
  { name: 'Executive Class(EC)', value: ec_orders },
  { name: 'AC 2 Tier(2A)', value: ac_2_orders },
  { name: 'First Class(FC)', value: fc_orders },
  { name: 'AC 3 Tier(3A)',value: ac_3_orders },
  { name: 'AC 3 Tier Economy(3E)', value: ac_3_e_orders },
  { name: 'AC Chair Car(CC)', value: ac_chair_orders },
  { name: 'Sleeper Class(SL)', value: sl_orders },
  { name: 'Second Sitting(2S)', value: ss_orders }

  ];
// sort by value
   sortClass.sort(function (a, b) {
    if (a.value > b.value) {
    return -1;
   }
   if (a.value < b.value) {
    return 1;
  }
  // a must be equal to b
  return 0;
   });
   
   whole_data['classes'] = sortClass;

  /* for(var i=0;i<sortClass.length;i++){
     console.log("name",sortClass[i].name);
     console.log("value",sortClass[i].value);
     console.log("  ");
   }    */
   

   var sortStatus = [
  { name: 'Confirm(CNF)', value: cnf_orders },
  { name: 'Reservation Against Cancellation(RAC)', value: rac_orders },
  { name: 'Tatkal Waiting List(CKWL)', value: ckwl_orders },
  { name: 'General waiting list(GNWL)', value: gnwl_orders },
  { name: 'Waiting list(WL)',value: wl_orders },
  { name: 'Pooled quota waiting list(PQWL)', value: pqwl_orders },
  { name: 'Remote location waiting list(RLWL)', value: rlwl_orders },
  { name: 'Remote Quota Waiting List(RQWL)', value: rqwl_orders }

  ];
// sort by value
   sortStatus.sort(function (a, b) {
    if (a.value > b.value) {
    return -1;
   }
   if (a.value < b.value) {
    return 1;
  }
  // a must be equal to b
  return 0;
   });

   whole_data['status'] = sortStatus;

/* '1A': 'First Class AC',
    'EC': 'Executive Class',
    '2A': 'AC 2 Tier',
    'FC': 'First Class',
    '3A': 'AC 3 Tier',
    '3E': 'AC 3 Tier Economy',
    'CC': 'AC Chair Car',
    'SL': 'Sleeper Class',
    '2S': 'Second Sitting'*/
    //whole_data['waiting_orders'] = waiting;
    /*whole_data['CNF'] = CNF.length;
    whole_data['CKWL'] = CKWL.length;
    whole_data['RQWL'] = RQWL.length;
    whole_data['RLWL'] = RLWL.l*/


    //console.log(waiting[0]);

    /*console.log("Cnf",CNF.length);
    console.log("ckwl",CKWL.length);
    console.log("rqwl",RQWL.length);



    /*for(k=0;k<waiting.length;k++)
    {
      if()
    }
    whole_data['waiting_orders'] = waiting_orders;
    whole_data['waiting_gmv'] = waiting_gmv;
    whole_data['waiting_tickets'] =waiting_tickets;*/


    /*whole_data['orders_with_promo_applied'] = null;
    whole_data['cashback_amount'] = null;
    whole_data['cashback_percentage'] = null;
    whole_data['pending_order'] = total_pending_order;
    whole_data['total_roundtrip_success_orders'] = total_roundtrip_success_order;
    whole_data['total_oneway_success_order'] = total_oneway_success_order;
    whole_data['total_oneway_tickets'] = total_oneway_tickets;
    whole_data['total_roundtrip_tickets'] = total_roundtrip_tickets;
    whole_data['total_oneway_failed_order'] = total_oneway_failed_order;
    whole_data['total_roundtrip_failed_order'] = total_roundtrip_failed_order;
    whole_data['total_oneway_failed_tickets'] = total_oneway_failed_tickets;
    whole_data['total_roundtrip_failed_tickets'] = total_roundtrip_failed_tickets;
    whole_data['total_oneway_success_tickets'] = total_oneway_success_tickets;
    whole_data['total_roundtrip_success_tickets'] = total_roundtrip_success_tickets;*/

 
    /*last_hour_data_with_partner['ezeego'] = createJSON(ezeego_details);
    last_hour_data_with_partner['emt'] = createJSON(emt_details);
    last_hour_data_with_partner['pearl'] = createJSON(pearl_details);*/
    data['whole_data'] = whole_data;
    //data['last_hour_data_with_partner'] = last_hour_data_with_partner;
    //data['pending_order_list_with_provider'] = pending_order_list_with_provider;
    //console.log(data);
    return data;
}



function collectstatus(orders){
  
    var data = {};

    
        whole_data = {};

      cnf_orders = 0;
      ckwl_orders = 0;
      rqwl_orders = 0;
      rac_orders = 0;
      rlwl_orders = 0;
      pqwl_orders = 0;
     

    for(var i=0; i<orders.length ; i++){
       
      try{
        if(orders[i]['status'] && orders[i]['status'] === 'SUCCESS'){
          if(orders[i].pax_info.booking_status && orders[i].pax_info.booking_status=== 'CNF')
            cnf_orders++;
          if(orders[i].pax_info.booking_status && orders[i].pax_info.booking_status=== 'CKWL')
            ckwl_orders++;
          if(orders[i].pax_info.booking_status && orders[i].pax_info.booking_status=== 'RQWL')
            rqwl_orders++;
          if(orders[i].pax_info.booking_status && orders[i].pax_info.booking_status=== 'RAC')
            rac_orders++;
          if(orders[i].pax_info.booking_status && orders[i].pax_info.booking_status=== 'RLWL')
            rlwl_orders++;
          if(orders[i].pax_info.booking_status && orders[i].pax_info.booking_status=== 'PQWL')
            pqwl_orders++;
        }

      } catch(err){
       // console.log(orders[i].order_id);
        }

    }
    
    whole_data['CNF'] = cnf_orders;
    whole_data['CKWL'] = ckwl_orders;
    whole_data['RQWL'] = rqwl_orders;
    whole_data['RLWL'] = rlwl_orders;
    whole_data['RAC'] = rac_orders;
    whole_data['PQWL'] =pqwl_orders;

    data['whole_data'] = whole_data;
   
    return data;
}


/*function createJSON(provider_info){
    var data = {};
    data['failed_gmv'] = provider_info.failed_gmv;
    data['gmv'] = provider_info.gmv;
    data['orders'] = provider_info.orders;
    data['tickets'] = provider_info.tickets;
    data['failed_order'] = provider_info.failed_orders;
    data['failed_tickets'] = provider_info.failed_tickets;
    data['cashback_percentage'] = null;
    data['pending_order'] = provider_info.pending_order;
    data['failed_gmv'] = provider_info.failed_gmv;
    data['roundtrip_success_orders'] = provider_info.roundtrip_success_orders;
    data['oneway_success_orders'] = provider_info.oneway_success_orders;
    data['roundtrip_tickets'] = provider_info.roundtrip_tickets;
    data['oneway_tickets'] = provider_info.oneway_tickets;
    data['roundtrip_failed_orders'] = provider_info.roundtrip_failed_orders;
    data['oneway_failed_orders'] = provider_info.oneway_failed_orders;
    data['roundtrip_failed_tickets'] = provider_info.roundtrip_failed_tickets;
    data['roundtrip_success_tickets'] = provider_info.roundtrip_success_tickets;
    data['oneway_success_tickets'] = provider_info.oneway_success_tickets;
    data['oneway_failed_tickets'] = provider_info.oneway_failed_tickets;
    return data;
}*/

function filter_system_wise_data(orders){
    var data = {};
      app = {};
      web = {};
      success_gmv_app = 0;
      success_orders_app = 0;
      success_tickets_app = 0;
      ac_gmv_app = 0;
      ac_orders_app = 0;
      ac_tickets_app = 0;
      failed_gmv_app = 0;
      failed_orders_app = 0;
      failed_tickets_app = 0;
      success_gmv_web = 0;
      success_orders_web = 0;
      success_tickets_web = 0;
      cancel_gmv_app = 0;
      cancel_orders_app = 0;
      cancel_tickets_app = 0;
      cancel_gmv_web = 0;
      cancel_orders_web = 0;
      cancel_tickets_web = 0;
      ac_gmv_web = 0;
      ac_orders_web = 0;
      ac_tickets_web = 0;
      failed_gmv_web = 0;
      failed_orders_web = 0;
      failed_tickets_web = 0;
    for(var i=0; i<orders.length ; i++){
        try{
        //var item = JSON.parse(orders[i].meta.order.items[0].meta_data);
          if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS'
             && orders[i]['booking_status'] && (orders[i]['booking_status']==='SUCCESS' || orders[i]['booking_status']==='CANCEL_REQUESTED')){
              if((orders[i].order.channel_id).indexOf('ANDROID')>-1 || (orders[i].order.channel_id).indexOf('IOS')>-1){
                  success_orders_app++;
                  success_tickets_app= success_tickets_app + orders[i]['order']['items'].length;
                  success_gmv_app = success_gmv_app + orders[i]['order']['grandtotal'];
                  if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                  //  console.log(orders[i]['meta']['query']['class']);
                    ac_orders_app++;
                    ac_tickets_app = ac_tickets_app + orders[i]['order']['items'].length;
                    ac_gmv_app = ac_gmv_app + orders[i]['order']['grandtotal'];
                  }
              }
              else{
                //  console.log(orders[i].order_id);
                  success_orders_web++;
                  success_tickets_web= success_tickets_web + orders[i]['order']['items'].length;
                  success_gmv_web = success_gmv_web + orders[i]['order']['grandtotal'];
                  if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                    console.log(orders[i]['meta']['query']['class']);
                    ac_orders_web++;
                    ac_tickets_web = ac_tickets_web + orders[i]['order']['items'].length;
                    ac_gmv_web = ac_gmv_web + orders[i]['order']['grandtotal'];
                  }

              }
          }if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS' 
           && orders[i]['booking_status'] && orders[i]['booking_status']==='FAILED'){
              if((orders[i].order.channel_id).indexOf('ANDROID')>-1 || (orders[i].order.channel_id).indexOf('IOS')>-1){
                  failed_orders_app++;
                  failed_tickets_app= failed_tickets_app + orders[i]['order']['items'].length;
                  failed_gmv_app = failed_gmv_app + orders[i]['order']['grandtotal'];
                  /*if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                    console.log(orders[i]['meta']['query']['class']);
                    ac_orders_app++;
                    ac_tickets_app = ac_tickets_app + orders[i]['order']['items'].length;
                    ac_gmv_app = ac_gmv_app + orders[i]['order']['grandtotal'];
                  }*/
              }
              else{
                  failed_orders_web++;
                  failed_tickets_web= failed_tickets_web + orders[i]['order']['items'].length;
                  failed_gmv_web = failed_gmv_web + orders[i]['order']['grandtotal'];
                  /*if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                    console.log(orders[i]['meta']['query']['class']);
                    ac_orders_web++;
                    ac_tickets_web = ac_tickets_web + orders[i]['order']['items'].length;
                    ac_gmv_web = ac_gmv_web + orders[i]['order']['grandtotal'];
                  }*/

              }
            }if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS' 
           && orders[i]['booking_status'] && orders[i]['booking_status']==='CANCELLED'){
              if((orders[i].order.channel_id).indexOf('ANDROID')>-1 || (orders[i].order.channel_id).indexOf('IOS')>-1){
                  cancel_orders_app++;
                  cancel_tickets_app= cancel_tickets_app + orders[i]['order']['items'].length;
                  cancel_gmv_app = cancel_gmv_app + orders[i]['order']['grandtotal'];
              }
              else{
                  cancel_orders_web++;
                  cancel_tickets_web= cancel_tickets_web + orders[i]['order']['items'].length;
                  cancel_gmv_web = cancel_gmv_web + orders[i]['order']['grandtotal'];

              }
            }
        } catch(err){}
    }


    app['success_gmv'] = success_gmv_app;
    app['success_order'] =success_orders_app;
    app['success_tickets'] = success_tickets_app;
    app['ac_gmv'] = ac_gmv_app;
    app['ac_orders'] = ac_orders_app;
    app['ac_tickets'] = ac_tickets_app;
    app['failed_gmv'] =failed_gmv_app;
    app['failed_orders'] =failed_orders_app;
    app['failed_tickets'] = failed_tickets_app;
    app['cancel_gmv'] =cancel_gmv_app;
    app['cancel_orders'] =cancel_orders_app;
    app['cancel_tickets'] = cancel_tickets_app;

    web['success_gmv'] = success_gmv_web;
    web['success_order'] =success_orders_web;
    web['success_tickets'] = success_tickets_web;
    web['ac_gmv'] = ac_gmv_web;
    web['ac_orders'] = ac_orders_web;
    web['ac_tickets'] = ac_tickets_web;
    web['failed_gmv'] =failed_gmv_web;
    web['failed_orders'] =failed_orders_web;
    web['failed_tickets'] = failed_tickets_web;
    web['cancel_gmv'] = cancel_gmv_web;
    web['cancel_orders'] = cancel_orders_web;
    web['cancel_tickets'] = cancel_tickets_web;


//    console.log('System Data is',app_details);
    data['app'] = app;
    data['web'] = web;
//    console.log('System Data is',data);
    return data; 
}



function filter_ac_wise_data(orders){
    var data = {};
      ac = {};
      non_ac = {};
      success_gmv_ac = 0;
      success_orders_ac = 0;
      success_tickets_ac = 0;
      failed_gmv_ac = 0;
      failed_orders_ac = 0;
      failed_tickets_ac = 0;
      success_gmv_non_ac = 0;
      success_orders_non_ac = 0;
      success_tickets_non_ac = 0;
      cancel_gmv_ac = 0;
      cancel_orders_ac = 0;
      cancel_tickets_ac = 0;
      cancel_gmv_non_ac = 0;
      cancel_orders_non_ac = 0;
      cancel_tickets_non_ac = 0;
      failed_gmv_non_ac = 0;
      failed_orders_non_ac = 0;
      failed_tickets_non_ac = 0;
    for(var i=0; i<orders.length ; i++){
        try{
        //var item = JSON.parse(orders[i].meta.order.items[0].meta_data);
          if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS'
             && orders[i]['booking_status'] && orders[i]['booking_status']==='SUCCESS'){
              if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                  success_orders_ac++;
                  success_tickets_ac= success_tickets_ac + orders[i]['order']['items'].length;
                  success_gmv_ac = success_gmv_ac + orders[i]['order']['grandtotal'];
  
              }
              else{
                  success_orders_non_ac++;
                  success_tickets_non_ac= success_tickets_non_ac + orders[i]['order']['items'].length;
                  success_gmv_non_ac = success_gmv_non_ac + orders[i]['order']['grandtotal'];
              }
          }if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS' 
           && orders[i]['booking_status'] && orders[i]['booking_status']==='FAILED'){
              if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                  failed_orders_ac++;
                  failed_tickets_ac= failed_tickets_ac + orders[i]['order']['items'].length;
                  failed_gmv_ac = failed_gmv_ac + orders[i]['order']['grandtotal'];
  
              }
              else{
                  failed_orders_non_ac++;
                  failed_tickets_non_ac= failed_tickets_non_ac + orders[i]['order']['items'].length;
                  failed_gmv_non_ac = failed_gmv_non_ac + orders[i]['order']['grandtotal'];
              }
            }if(orders[i]['payment_status'] && orders[i]['payment_status'] === 'SUCCESS' 
           && orders[i]['booking_status'] && orders[i]['booking_status']==='CANCELLED'){
              if(orders[i]['meta']['query']['class']!='2S' && orders[i]['meta']['query']['class']!='SL'){
                  cancel_orders_ac++;
                  cancel_tickets_ac= cancel_tickets_ac + orders[i]['order']['items'].length;
                  cancel_gmv_ac = cancel_gmv_ac + orders[i]['order']['grandtotal'];
  
              }
              else{
                  cancel_orders_non_ac++;
                  cancel_tickets_non_ac= cancel_tickets_non_ac + orders[i]['order']['items'].length;
                  cancel_gmv_non_ac = cancel_gmv_non_ac + orders[i]['order']['grandtotal'];
              }
            }
        } catch(err){}
    }


    ac['success_gmv'] = success_gmv_ac;
    ac['success_order'] =success_orders_ac;
    ac['success_tickets'] = success_tickets_ac;
    ac['failed_gmv'] =failed_gmv_ac;
    ac['failed_orders'] =failed_orders_ac;
    ac['failed_tickets'] = failed_tickets_ac;
    ac['cancel_gmv'] =cancel_gmv_ac;
    ac['cancel_orders'] =cancel_orders_ac;
    ac['cancel_tickets'] = cancel_tickets_ac;

    non_ac['success_gmv'] = success_gmv_non_ac;
    non_ac['success_order'] =success_orders_non_ac;
    non_ac['success_tickets'] = success_tickets_non_ac;
    non_ac['failed_gmv'] =failed_gmv_non_ac;
    non_ac['failed_orders'] =failed_orders_non_ac;
    non_ac['failed_tickets'] = failed_tickets_non_ac;
    non_ac['cancel_gmv'] = cancel_gmv_non_ac;
    non_ac['cancel_orders'] = cancel_orders_non_ac;
    non_ac['cancel_tickets'] = cancel_tickets_non_ac;


//    console.log('System Data is',ac_details);
    data['ac'] = ac;
    data['non_ac'] = non_ac;
//    console.log('System Data is',data);
    return data; 
}

/*function createSystemJSON(system_info){
    var data = {};
    data['failed_gmv'] = system_info.failed_gmv;
    data['gmv'] = system_info.gmv;
    data['orders'] = system_info.order;
    data['tickets'] = system_info.tickets;
    data['failed_order'] = system_info.failed_order;
    data['failed_tickets'] = system_info.failure_tickets;
    data['cashback_percentage'] = null;
    data['pending_order'] = system_info.pending_order;
    data['roundtrip_success_orders'] = system_info.roundtrip_success_order;
    data['oneway_success_orders'] = system_info.oneway_success_order;
    data['roundtrip_tickets'] = system_info.roundtrip_tickets;
    data['oneway_tickets'] = system_info.oneway_tickets;
    data['roundtrip_failed_orders'] = system_info.roundtrip_failed_order;
    data['oneway_failed_orders'] = system_info.oneway_failed_order;
    data['roundtrip_failed_tickets'] = system_info.roundtrip_failed_tickets;
    data['roundtrip_success_tickets'] = system_info.roundtrip_success_tickets;
    data['oneway_success_tickets'] = system_info.oneway_success_tickets;
    data['oneway_failed_tickets'] = system_info.oneway_failed_tickets;
    return data;
}*/

